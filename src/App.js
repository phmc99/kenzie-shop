import { Toaster } from "react-hot-toast";
import Routes from "./routes/Routes";
import "./styles/style.css";

const App = () => {
  return (
    <>
      <Toaster />
      <Routes />
    </>
  );
};

export default App;
