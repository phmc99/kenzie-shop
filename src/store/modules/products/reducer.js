const products = [
  {
    name: "Echo Dot",
    price: 279.0,
    image:
      "https://lojaibyte.vteximg.com.br/arquivos/ids/206885-540-540/amazon-echo-dot-4-geracao-smart-speaker-com-alexa-preto-01.jpg?v=637384720240070000",
  },
  {
    name: "Kindle Paperwhite",
    price: 499.0,
    image:
      "https://novomundo.vteximg.com.br/arquivos/ids/1267381-500-500/kindle-paperwhite-amazon-6-4gb-wi-fi-iluminacao-embutida-preto-kindle-paperwhite-amazon-6-4gb-wi-fi-iluminacao-embutida-preto-57077-2.jpg?v=636746966696800000",
  },
];

const productsReducer = (state = products, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default productsReducer;
