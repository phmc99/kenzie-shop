import { ADD_TO_CART, REMOVE_FROM_CART } from "./actionsTypes";

export const addProduct = (product) => ({
  type: ADD_TO_CART,
  product,
});

export const removeProduct = (product) => ({
  type: REMOVE_FROM_CART,
  product,
});
