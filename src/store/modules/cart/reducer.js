import { ADD_TO_CART, REMOVE_FROM_CART } from "./actionsTypes";
const cart = JSON.parse(localStorage.getItem("cart")) || [];

const cartReducer = (state = cart, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      return [...state, action.product];
    case REMOVE_FROM_CART:
      const newCart = state.filter((item) => item.name !== action.product.name);
      return newCart;
    default:
      return state;
  }
};

export default cartReducer;
