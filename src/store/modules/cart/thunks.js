import { addProduct, removeProduct } from "./actions";

export const addProductThunk = (product) => {
  return (dispatch) => {
    const cart = JSON.parse(localStorage.getItem("cart")) || [];
    const productList = [...cart, product];

    localStorage.setItem("cart", JSON.stringify(productList));
    dispatch(addProduct(product));
  };
};

export const removeProductThunk = (product) => {
  return (dispatch) => {
    const cart = JSON.parse(localStorage.getItem("cart")) || [];
    const filteredList = cart.filter((item) => item.name !== product.name);

    localStorage.setItem("cart", JSON.stringify(filteredList));
    dispatch(removeProduct(product));
  };
};
