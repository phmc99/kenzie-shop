import "./style.css";

const Button = ({ func, children }) => {
  return (
    <button onClick={func} className="global-button">
      {children}
    </button>
  );
};

export default Button;
