import "./style.css";

const ProductCard = ({ name, price, img }) => {
  return (
    <div className="product-card">
      <h2>{name}</h2>
      <span>R${price}</span>
      <img src={img} alt="" />
    </div>
  );
};

export default ProductCard;
