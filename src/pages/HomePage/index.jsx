import { useDispatch, useSelector } from "react-redux";
import ProductCard from "../../components/ProductCard";
import Button from "../../components/Button";
import "./style.css";
import { addProductThunk } from "../../store/modules/cart/thunks";
import toast from "react-hot-toast";
import { useHistory } from "react-router-dom";

const HomePage = () => {
  const history = useHistory();

  const products = useSelector((state) => state.products);
  const cart = useSelector((state) => state.cart);

  const dispatch = useDispatch();

  const handleClick = (product) => {
    if (cart.includes(product)) {
      toast.error(`Esse produto já foi adicionado ao carrinho`);
    } else {
      toast.success(`Esse produto foi adicionado ao carrinho`);

      dispatch(addProductThunk(product));
    }
  };

  const seeCart = () => {
    history.push("/cart");
  };

  return (
    <div className="products-container">
      <Button func={seeCart}>Ver carrinho</Button>
      <ul className="product-list">
        {products.map((item, index) => (
          <li key={index}>
            <ProductCard name={item.name} price={item.price} img={item.image} />
            <Button func={() => handleClick(item)}>
              Adicionar ao carrinho
            </Button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default HomePage;
