import { useDispatch, useSelector } from "react-redux";
import ProductCard from "../../components/ProductCard";
import toast from "react-hot-toast";
import { useHistory } from "react-router-dom";
import Button from "../../components/Button";

import { removeProductThunk } from "../../store/modules/cart/thunks";

const CartPage = () => {
  const cart = useSelector((state) => state.cart);
  const history = useHistory();
  const dispatch = useDispatch();

  const seeHome = () => {
    history.push("/");
  };

  const handleClick = (product) => {
    dispatch(removeProductThunk(product));
    toast.error(`Esse produto foi removido do carrinho`);
  };

  return (
    <div className="products-container">
      <Button func={seeHome}>Voltar para a home</Button>
      {cart.length !== 0 ? (
        <>
          <ul className="product-list">
            {cart.map((item, index) => (
              <li key={index}>
                <ProductCard
                  name={item.name}
                  price={item.price}
                  img={item.image}
                />
                <Button func={() => handleClick(item)}>Remover</Button>
              </li>
            ))}
          </ul>
          <span style={{ color: "#f3f3f3" }}>
            Valor total: R${cart.reduce((acc, item) => acc + item.price, 0)}
          </span>
        </>
      ) : (
        <h1 style={{ color: "#f3f3f3" }}>Carrinho vazio</h1>
      )}
    </div>
  );
};

export default CartPage;
